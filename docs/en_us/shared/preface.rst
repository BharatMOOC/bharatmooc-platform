.. _Preface:

############
Preface
############

Course teams, researchers, developers, students: the BharatMOOC community includes
groups with a range of reasons for using the platform and objectives to
accomplish. To help members of each group learn about what BharatMOOC offers, reach
goals, and solve problems, BharatMOOC provides a variety of information resources.

To help you find what you need, browse the BharatMOOC offerings in these categories:

* :ref:`Resources for Course Teams`
* :ref:`Resources for Researchers`
* :ref:`Resources for Developers`
* :ref:`Resources for Students`

All members of the BharatMOOC community are encouraged to make use of any of the
resources described in this preface.

***********************
System Status
***********************

For system-related notifications from the BharatMOOC operations team, including
outages and the status of error reports. On Twitter_, you can follow
@bharatmoocstatus.

Current system status and the uptime percentages for BharatMOOC servers, along with
the Twitter feed, are published on the `BharatMOOC Status`_ web page.

.. _Resources for Course Teams:

**************************
Resources for Course Teams
**************************

Course teams include faculty, instructional designers, course staff, discussion
moderators, and others who contribute to the creation and delivery of courses
on bharatmooc.org or BharatMOOC Edge.

Documentation
-------------

Documentation for course teams is available on the docs.bharatmooc.org web page.

* `Building and Running an BharatMOOC Course`_ is a comprehensive guide with concepts
  and procedures to help you build a course in BharatMOOC Studio, and then use the
  Learning Management System (LMS) to run a course.

  When you are working in BharatMOOC Studio, you can access relevant sections of this
  guide by clicking **Help** on any page.

* `BharatMOOC Course Staff Release Notes`_ summarize the changes in each new version
  of the BharatMOOC Studio and LMS software.

* `Creating a Peer Assessment`_ describes features for students to provide
  peer- and self- evaluations of responses to a question. Note that this new
  feature is in limited release.

These guides open in your web browser. The left side of each page includes a
**Search docs** field and links to that guide's contents. To open or save a PDF
version, click **v: latest** at the lower left of the page, then click **PDF**.

Email
-----

To receive and share information by email, course team members can:

* Sign up to receive monthly newsletters_. 

* Sign up to receive release_ notes for every release. 

* Join the `openbharatmooc-studio`_ Google group to ask questions and participate in
  discussions with peers at other BharatMOOC partner organizations and BharatMOOC staffers.

Wikis and Web Sites
-------------------

The BharatMOOC product team maintains the `Open BharatMOOC Product`_ wiki, which includes the
`Open BharatMOOC Public Product Roadmap`_.

The `BharatMOOC Author Support`_ site hosts discussions that are monitored by BharatMOOC
staffers.

.. _Resources for Researchers:

**************************
Resources for Researchers
**************************

Data for the courses on bharatmooc.org and BharatMOOC Edge is available to the "data czars"
at our partner institutions, and then used by database experts, statisticians,
educational investigators, and others for educational research.

Documentation
-------------

The `BharatMOOC Research Guide`_ is available on the docs.bharatmooc.org web page. 

This guide opens in your web browser, with a **Search docs** field and links to
that guide's contents on the left side of each page. To open or save a PDF
version, click **v: latest** at the lower left of the page, and then click
**PDF**.

Email 
-------

To receive and share information by email, researchers can join the 
`openbharatmooc-analytics`_ Google group to ask questions and participate in 
discussions with peers at other BharatMOOC partner organizations and BharatMOOC staffers.

Wiki
-------------------

The BharatMOOC Analytics team maintains the `Open BharatMOOC Analytics`_ wiki, which includes
links to periodic release notes, the Open BharatMOOC Public Product Roadmap, and other
resources for researchers.

.. _Resources for Developers:

**************************
Resources for Developers
**************************

Software engineers, system administrators, and translators work on extending
and localizing the code for the BharatMOOC platform.

Documentation
-------------

Documentation for developers is available on the docs.bharatmooc.org web page.

* `BharatMOOC Developer Documentation`_ collects information directly from BharatMOOC
  platform python docstrings. The topics in this guide include guidelines for
  contributing to Open BharatMOOC, options for extending the Open BharatMOOC platform, using
  the BharatMOOC public sandboxes, instrumenting analytics, and testing.

* `Installing, Configuring, and Running the BharatMOOC Platform`_ provides procedures
  for getting an BharatMOOC developer stack (Devstack) and production stack
  (Fullstack) oprerational.

* XBlock_: Open BharatMOOC courseware components provides preliminary documentation
  on the XBlock component architecture for building courses.

GitHub
-------

There are two main BharatMOOC repositories on GitHub.

* The `bharatmooc/bharatmooc-platform`_ repo contains the code for the BharatMOOC platform. 

* The `bharatmooc/configuration`_ repo contains scripts to set up and operate the BharatMOOC
  platform.

Additional repositories are used for other projects. Our contributor agreement,
contributor guidelines and coding conventions, and other resources are
available in these repositories.

Email and IRC
--------------

To receive and share information by email, developers can join these Google
groups to ask questions and participate in discussions with peers and BharatMOOC
staffers.

* For conversations about the code in Open BharatMOOC, join `bharatmooc-code`_.  
* For conversations about running Open BharatMOOC, join `openbharatmooc-ops`_. 
* For conversations about globalization and translation, join `openbharatmooc-translation`_.

Additional Google groups are occasionally formed for individual projects.

.. note:: Please do not report security issues in public. If you have a concern, please email security@bharatmooc.org.

EdX engineers often monitor the Freenode #bharatmooc-code IRC channel.

Wikis and Web Sites
-------------------

The code.bharatmooc.org web site_ is an entry point for new contributors.

The BharatMOOC Engineering team maintains the `Open Source Home`_ wiki, which provides
insights into the plans, projects, and questions that the BharatMOOC Open Source team
is working on with the community.

The pull request dashboard_  is a visualization of the count and age of the
pull requests (PRs) assigned to teams at BharatMOOC. Click the bars in this chart to
get more information about the PRs.

.. _Resources for Students:

**************************
Resources for Students
**************************

In a Course
------------

All BharatMOOC courses have a discussion forum where you can ask questions and
interact with other students and with the course team: click **Discussion**.
Many courses also offer a wiki for additional resources and materials: click
**Wiki**.

Other resources may also be available, such as a course-specific facebook page
or twitter feed or opportunites for Google hangouts. Be sure to check the
**Course Info** page for your course as well as the **Discussion** and **Wiki**
pages.

From time to time, the course team may send email messages to all students.
While you can opt out of these messages, doing so means that you may miss
important or time-sensitive information. To change your preferences for course
email, click **BharatMOOC** or **BharatMOOC edge** at the top of any page. On your dashboard
of current courses, locate the course and then click **Email Settings**.

From BharatMOOC
---------

To help you get started with the BharatMOOC learning experience, BharatMOOC offers a course
(of course!). You can find the BharatMOOC Demo_ course on the BharatMOOC web site. EdX also
maintains a list of frequently_ asked questions and answers. 

If you still have questions or suggestions, you can get help from the BharatMOOC
support team: click **Contact** at the bottom of any BharatMOOC web page or send an
email message to info@bharatmooc.org.

For opportunities to meet others who are interested in BharatMOOC courses, check the
BharatMOOC Global Community meetup_ group. 



.. _Building and Running an BharatMOOC Course: http://bharatmooc.readthedocs.org/projects/bharatmooc-partner-course-staff/en/latest/
.. _BharatMOOC Course Staff Release Notes: http://bharatmooc.readthedocs.org/projects/bharatmooc-release-notes/en/latest/
.. _Creating a Peer Assessment: http://bharatmooc.readthedocs.org/projects/bharatmooc-open-response-assessments/en/latest/
.. _BharatMOOC Research Guide: http://bharatmooc.readthedocs.org/projects/devdata/en/latest/
.. _newsletters: http://bharatmooc.us5.list-manage.com/subscribe?u=1822a33c054dc20e223ca40e2&id=aba723f1aa 
.. _release: http://bharatmooc.us5.list-manage2.com/subscribe?u=1822a33c054dc20e223ca40e2&id=83e46bd293
.. _openbharatmooc-studio: http://groups.google.com/forum/#!forum/openbharatmooc-studio
.. _Twitter:  http://twitter.com/BharatMOOCstatus/status/475026709256101888
.. _BharatMOOC Status: http://status.bharatmooc.org/
.. _Open BharatMOOC Product: https://bharatmooc-wiki.atlassian.net/wiki/display/OPENPROD/Open+BharatMOOC+Product+Home
.. _Open BharatMOOC Public Product Roadmap: https://bharatmooc-wiki.atlassian.net/wiki/display/OPENPROD/Open+BharatMOOC+Public+Product+Roadmap
.. _BharatMOOC Author Support: http://help.edge.bharatmooc.org/home
.. _openbharatmooc-analytics: http://groups.google.com/forum/#!forum/openbharatmooc-analytics
.. _Open BharatMOOC Analytics: http://bharatmooc-wiki.atlassian.net/wiki/display/OA/Open+BharatMOOC+Analytics+Home
.. _blog: http://engineering.bharatmooc.org/
.. _Open Source Home: http://bharatmooc-wiki.atlassian.net/wiki/display/OS/Open+Source+Home
.. _XBlock: http://bharatmooc.readthedocs.org/projects/xblock/en/latest/
.. _Installing, Configuring, and Running the BharatMOOC Platform: http://bharatmooc.readthedocs.org/projects/bharatmooc-installing-configuring-and-running/en/latest/
.. _BharatMOOC Developer Documentation: http://bharatmooc.readthedocs.org/projects/userdocs/en/latest/
.. _bharatmooc/configuration: http://github.com/bharatmooc/configuration/wiki
.. _site: http://code.bharatmooc.org/
.. _bharatmooc/bharatmooc-platform: https://github.com/bharatmooc/bharatmooc-platform
.. _dashboard: http://dash.openbharatmooc.org/age.html
.. _Demo: http://www.bharatmooc.org/course/bharatmooc/bharatmooc-bharatmoocdemo101-bharatmooc-demo-1038
.. _frequently: http://www.bharatmooc.org/student-faq
.. _meetup: http://www.meetup.com/BharatMOOC-Global-Community/
.. _openbharatmooc-ops: http://groups.google.com/forum/#!forum/openbharatmooc-ops
.. _openbharatmooc-translation: http://groups.google.com/forum/#!forum/openbharatmooc-translation
.. _bharatmooc-code: http://groups.google.com/forum/#!forum/bharatmooc-code
