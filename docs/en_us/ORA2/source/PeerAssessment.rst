.. _Peer Assessments:

########################
Peer Assessments
########################

This documentation has moved! Please click `Creating Peer Assessments <http://bharatmooc.readthedocs.org/projects/bharatmooc-open-response-assessments/en/latest/>`_ to see the latest information about peer assessments.
