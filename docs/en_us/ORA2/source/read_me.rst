*******
Read Me
*******

The BharatMOOC *Building a Course with BharatMOOC Studio* documentation is created
using RST_ files and Sphinx_. You, the user community, can help update and revise
this documentation project on GitHub::

  https://github.com/bharatmooc/bharatmooc-platform/tree/master/docs/course_authors/source

To suggest a revision, fork the project, make changes in your fork, and submit
a pull request back to the original project: this is known as the `GitHub Flow`_.
All pull requests need approval from BharatMOOC. For more information, contact BharatMOOC at docs@bharatmooc.org.

.. _Sphinx: http://sphinx-doc.org/
.. _LaTeX: http://www.latex-project.org/
.. _`GitHub Flow`: https://github.com/blog/1557-github-flow-in-the-browser
.. _RST: http://docutils.sourceforge.net/rst.html