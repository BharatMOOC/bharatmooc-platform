.. _PA for Students:

#############################
Peer Assessments for Students
#############################

This documentation has moved! Please click `Peer Assessments for Students <http://bharatmooc.readthedocs.org/projects/bharatmooc-open-response-assessments/en/latest/PeerAssessment_Students.html>`_ to see the latest peer assessment information for students.