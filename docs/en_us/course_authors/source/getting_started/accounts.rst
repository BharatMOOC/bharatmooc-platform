.. _Getting Started with BharatMOOC:

#############################
Getting Started with BharatMOOC
#############################

The following sections provide an introduction to BharatMOOC and instructions for getting started on BharatMOOC websites:

* :ref:`BharatMOOC.org and BharatMOOC Edge`
* :ref:`Register Your Account`
* :ref:`BharatMOOC101_section`
* :ref:`Reset Your Password`

This information is intended for course staff. You may want to include the details about the registration process and password policies in your communications with your prospective students. 

Also see :ref:`enroll_student` for information about enrolling students in a course, and for an example of the email students receive.

.. _BharatMOOC.org and BharatMOOC Edge:

*************************
BharatMOOC.org and BharatMOOC Edge
*************************

When you want to explore BharatMOOC courses, you can register on BharatMOOC.org_, BharatMOOC Edge_, or both. These sites are visually and functionally the same, but the content and purpose are different.

* BharatMOOC.org_ hosts massive open online courses (MOOCs) from BharatMOOC institutional partners. To publish courses on BharatMOOC.org, you must have an agreement with BharatMOOC and specific approval from your university. Courses on BharatMOOC.org are publicly listed in the BharatMOOC course catalog and are open to students from around the world.

* EdX Edge_ is BharatMOOC's more private site. Courses on Edge are not published on BharatMOOC.org. Any member of a partner course team can create and publish courses, including test courses, on Edge without receiving approval from BharatMOOC or an affiliated university. However, Edge does not have a course catalog, and courses cannot be found through search engines such as Google. Only students whom you explicitly invite or who have the URL for your course can participate in your course on Edge.

* EdX Edge_ also hosts small private online courses (SPOCs).

.. note:: All course data and accounts on Edge_ and BharatMOOC.org_ are separate. If you want to use both BharatMOOC.org and Edge, you must go through the registration process on both sites.


.. _Edge: http://edge.bharatmooc.org
.. _BharatMOOC.org: http://bharatmooc.org



.. _Register Your Account:

*************************
Register Your Account
*************************

To get started, you register your account on BharatMOOC.org_ and Edge_.  

#. On the registration page, enter your account information.

  * For BharatMOOC.org, go to the `registration page <https://courses.bharatmooc.org/register>`_:

    .. image:: ../Images/bharatmooc_registration.png
     :alt: Image of the BharatMOOC.org registration page


  * For Edge, go to https://edge.bharatmooc.org and click **Register**:

    .. image:: ../Images/edge_register.png
     :alt: Image of the Edge registration page

  Make sure to check both **I agree to the Terms of Service** and **I agree to the Honor Code**.  

  .. note::  Students will see your **Public Username**, not your **Full Name**.

    If you are at an BharatMOOC consortium university, you should use your institutional e-mail address.

    Your password can be any string.

2. When you complete the form, click **Create my BharatMOOC Account**.

  After you submit the registration, you receive an activation email message. The email content is::

    Thank you for signing up for BharatMOOC! 
    To activate your account, please copy and paste this address into your web 
    browser's address bar:

    https://courses.bharatmooc.org/activate/unique-registration-code
  
    If you didn't request this, you don't need to do anything; 
    you won't receive any more email from us. 
    Please do not reply to this e-mail; if you require assistance, 
    check the help section of the BharatMOOC web site.

3. Click the link in the e-mail to complete the activation. When you see the following page, your account has been activated:

  .. image:: ../Images/activation_screen.png
   :alt: Image of the Activation page


.. _BharatMOOC101_section:

******************************
BharatMOOC101 and the BharatMOOC Demo Course
******************************

EdX has provided BharatMOOC101_ and the `BharatMOOC Demo course <https://www.bharatmooc.org/course/bharatmooc/bharatmooc-bharatmoocdemo101-bharatmooc-demo-1038>`_ to help familiarize you with taking and creating BharatMOOC courses.

* BharatMOOC101_, available on Edge_, is an example of a course built with Studio. It is a self-paced walk through of planning, building, and running your own online course.

* The `BharatMOOC Demo course <https://www.bharatmooc.org/course/bharatmooc/bharatmooc-bharatmoocdemo101-bharatmooc-demo-1038>`_, available on BharatMOOC.org_, allows new students to explore and learn how to take an BharatMOOC course. We recommend that you become familiar with the student’s experience of an BharatMOOC course before you begin building your first course.

.. note:: You may want to include information about the BharatMOOC Demo Course in your course materials, and recommend that new students take the BharatMOOC Demo Course before proceeding with your course.

.. _bharatmooc101: https://edge.bharatmooc.org/courses/BharatMOOC/BharatMOOC101/How_to_Create_an_BharatMOOC_Course/about


.. _Reset Your Password:

*******************
Reset Your Password
*******************

The process to reset your password on BharatMOOC.org and Edge is the same.

#. On bharatmooc.org or edge.bharatmooc.org, go to your Dashboard.

#. In the account information pane in the upper left corner, click **Reset Password**. 

  .. image:: ../Images/dashboard-password-reset.png
   :alt: Image with the Reset Password link highlighted

  A dialog box opens confirming that a message has been sent to your email address.
 
  .. image:: ../Images/password-email-dialog.png
   :alt: Image with the Reset Password link highlighted

3. When you receive the following e-mail message, click the link in the message::

     You're receiving this e-mail because you requested a 
     password reset for your user account at bharatmooc.org.

     Please go to the following page and choose a new password:

     https://bharatmooc.org/password_reset_confirm/unique-code/

     If you didn't request this change, you can disregard this email - 
     we have not yet reset your password.

     Thanks for using our site!

     The BharatMOOC Team

4. When the following dialog box opens, enter your new password in both fields, and then click **Change My Password**:

  .. image:: ../Images/reset_password.png
   :alt: Image of the Reset Password dialog box

  .. note:: Your password can be any string.

After you click **Change My Password**, your password is reset for BharatMOOC.org or edge.BharatMOOC.org. You must use the new password when you log in.