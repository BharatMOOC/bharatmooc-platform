  
.. Links

.. _BharatMOOC Release Announcements mailing list: http://eepurl.com/U84tj

.. _BharatMOOC Edge: https://edge.bharatmooc.org

.. _bharatmooc.org: http://bharatmooc.org

.. _Sphinx: http://sphinx-doc.org/
.. _LaTeX: http://www.latex-project.org/
.. _GitHub Flow: https://github.com/blog/1557-github-flow-in-the-browser
.. _RST: http://docutils.sourceforge.net/rst.html

.. _BharatMOOC Status: http://status.bharatmooc.org/

.. _BharatMOOC roadmap: https://bharatmooc-wiki.atlassian.net/wiki/display/OPENPROD/Open+EdX+Public+Product+Roadmap

.. _Building and Running an BharatMOOC Course: http://bharatmooc.readthedocs.org/projects/bharatmooc-partner-course-staff/en/latest/

.. _Installing, Configuring, and Running the BharatMOOC Platform: http://bharatmooc.readthedocs.org/projects/bharatmooc-installing-configuring-and-running/en/latest/

.. _Installing the BharatMOOC Production Stack: http://bharatmooc.readthedocs.org/projects/bharatmooc-installing-configuring-and-running/en/latest/prodstack/install_prodstack.html

.. _Creating Peer Assessments: http://bharatmooc.readthedocs.org/projects/bharatmooc-open-response-assessments/en/latest/

.. _BharatMOOC Research Guide: http://bharatmooc.readthedocs.org/projects/devdata/en/latest/

.. _BharatMOOC Developer's Guide: http://bharatmooc.readthedocs.org/projects/userdocs/en/latest/

.. _Testing Your Course: http://bharatmooc.readthedocs.org/projects/bharatmooc-partner-course-staff/en/latest/releasing_course/view_course_content.html#testing-your-course

.. _Switch Between Studio and Your Live Course: http://bharatmooc.readthedocs.org/projects/bharatmooc-partner-course-staff/en/latest/releasing_course/view_course_content.html#view-your-live-course

.. _Transcripts in Additional Languages: http://bharatmooc.readthedocs.org/projects/bharatmooc-partner-course-staff/en/latest/creating_content/create_video.html#transcripts-in-additional-languages

.. _Working with Video Components: http://bharatmooc.readthedocs.org/projects/bharatmooc-partner-course-staff/en/latest/creating_content/create_video.html

.. _Bulk Email: http://bharatmooc.readthedocs.org/projects/bharatmooc-partner-course-staff/en/latest/releasing_course/course_launching.html#bulk-email

.. _Enrollment: http://ca.readthedocs.org/en/latest/running_course/course_enrollment.html#enrollment

.. _Shuffle Answers in a Multiple Choice Problem: http://bharatmooc.readthedocs.org/projects/bharatmooc-partner-course-staff/en/latest/exercises_tools/multiple_choice.html#shuffle-answers-in-a-multiple-choice-problem

.. _Targeted Feedback in a Multiple Choice Problem: http://bharatmooc.readthedocs.org/projects/bharatmooc-partner-course-staff/en/latest/exercises_tools/multiple_choice.html#targeted-feedback-in-a-multiple-choice-problem

.. _Answer Pools in a Multiple Choice Problem: http://bharatmooc.readthedocs.org/projects/bharatmooc-partner-course-staff/en/latest/exercises_tools/multiple_choice.html#answer-pools-in-a-multiple-choice-problem

.. _Grade and Answer Data: http://bharatmooc.readthedocs.org/projects/bharatmooc-partner-course-staff/en/latest/running_course/course_grades.html#grades

.. _Multiple Choice: http://bharatmooc.readthedocs.org/projects/bharatmooc-partner-course-staff/en/latest/exercises_tools/multiple_choice.html

.. _Course Data: http://bharatmooc.readthedocs.org/projects/bharatmooc-partner-course-staff/en/latest/running_course/course_data.html#course-data

.. _Staffing: http://bharatmooc.readthedocs.org/projects/bharatmooc-partner-course-staff/en/latest/running_course/course_staffing.html#course-staffing

.. _Working with HTML Components: http://bharatmooc.readthedocs.org/projects/bharatmooc-partner-course-staff/en/latest/creating_content/create_html_component.html

.. _Custom JavaScript Applications: http://bharatmooc.readthedocs.org/projects/userdocs/en/latest/extending_platform/javascript.html

.. _Go to Studio from Your Live Course: http://bharatmooc.readthedocs.org/projects/bharatmooc-partner-course-staff/en/latest/releasing_course/view_course_content.html#go-to-studio-from-your-live-course

.. _Student Answer Distribution: http://bharatmooc.readthedocs.org/projects/bharatmooc-partner-course-staff/en/latest/running_course/course_grades.html#review-answers

.. _Releasing Your Course: http://ca.readthedocs.org/en/latest/releasing_course/index.html#releasing-your-course-index

.. _Course Launching Activities: http://ca.readthedocs.org/en/latest/releasing_course/course_launching.html#launch

.. _Adding Pages to a Course: http://bharatmooc.readthedocs.org/projects/bharatmooc-partner-course-staff/en/latest/creating_content/pages.html#adding-pages-to-a-course
    
.. _Show or Hide the Course Wiki Page: http://bharatmooc.readthedocs.org/projects/bharatmooc-partner-course-staff/en/latest/creating_content/pages.html#show-or-hide-the-course-wiki-page

.. _Building a Course: http://bharatmooc.readthedocs.org/projects/bharatmooc-partner-course-staff/en/latest/building_course/index.html#building-a-course-index 

.. _Creating Course Content: http://bharatmooc.readthedocs.org/projects/bharatmooc-partner-course-staff/en/latest/creating_content/index.html#creating-course-content-index

.. _Beta Testing a Course: http://bharatmooc.readthedocs.org/projects/bharatmooc-partner-course-staff/en/latest/releasing_course/beta_testing.html#beta-testing

.. _Student Data: http://bharatmooc.readthedocs.org/projects/bharatmooc-partner-course-staff/en/latest/running_course/course_student.html#student-data

.. _Add the Beta Testers: http://bharatmooc.readthedocs.org/projects/bharatmooc-partner-course-staff/en/latest/releasing_course/beta_testing.html#add-testers

.. _Getting Started: http://bharatmooc.readthedocs.org/projects/bharatmooc-partner-course-staff/en/latest/getting_started/index.html#getting-started-index

.. _Building a Course: http://bharatmooc.readthedocs.org/projects/bharatmooc-partner-course-staff/en/latest/building_course/index.html#building-a-course-index


.. _Creating Exercises and Tools: http://bharatmooc.readthedocs.org/projects/bharatmooc-partner-course-staff/en/latest/exercises_tools/index.html

.. _Running Your Course: http://bharatmooc.readthedocs.org/projects/bharatmooc-partner-course-staff/en/latest/running_course/index.html#running-your-course-index

.. _Information for Your Students: http://bharatmooc.readthedocs.org/projects/bharatmooc-partner-course-staff/en/latest/students/index.html#information-for-your-students-index



.. _The Course Start Date: http://bharatmooc.readthedocs.org/projects/bharatmooc-partner-course-staff/en/latest/building_course/setting_up_student_view.html#the-course-start-date

.. _The Course End Date: http://bharatmooc.readthedocs.org/projects/bharatmooc-partner-course-staff/en/latest/building_course/setting_up_student_view.html#the-course-end-date

.. _Add a Course Image: http://bharatmooc.readthedocs.org/projects/bharatmooc-partner-course-staff/en/latest/building_course/setting_up_student_view.html#add-a-course-image

.. _Add a Course Introduction Video: http://bharatmooc.readthedocs.org/projects/bharatmooc-partner-course-staff/en/latest/building_course/setting_up_student_view.html#add-a-course-video

.. _Describe Your Course: http://bharatmooc.readthedocs.org/projects/bharatmooc-partner-course-staff/en/latest/building_course/setting_up_student_view.html#describe-your-course

.. _Create or Obtain a Video Transcript: http://bharatmooc.readthedocs.org/projects/bharatmooc-partner-course-staff/en/latest/creating_content/create_video.html#create-transcript

.. _Staff Debug Info: http://bharatmooc.readthedocs.org/projects/bharatmooc-partner-course-staff/en/latest/releasing_course/staff_debug_info.html

.. _Add Files to a Course: http://bharatmooc.readthedocs.org/projects/bharatmooc-partner-course-staff/en/latest/creating_content/course_files.html

.. _Import LaTeX Code into an HTML Component: http://bharatmooc.readthedocs.org/projects/bharatmooc-partner-course-staff/en/latest/creating_content/create_html_component.html#import-latex-code

.. _Establishing a Grading Policy: http://bharatmooc.readthedocs.org/projects/bharatmooc-partner-course-staff/en/latest/building_course/establish_grading_policy.html


.. _Additional transcripts: http://bharatmooc.readthedocs.org/projects/bharatmooc-partner-course-staff/en/latest/creating_content/create_video.html#additional-transcripts

.. _Staffing data: http://bharatmooc.readthedocs.org/projects/bharatmooc-partner-course-staff/en/latest/running_course/course_staffing.html

.. _Enrollment data: http://bharatmooc.readthedocs.org/projects/bharatmooc-partner-course-staff/en/latest/running_course/course_enrollment.html

.. _Gene Explorer: http://bharatmooc.readthedocs.org/projects/bharatmooc-partner-course-staff/en/latest/exercises_tools/gene_explorer.html

.. _Problem Written in LaTeX: http://bharatmooc.readthedocs.org/projects/bharatmooc-partner-course-staff/en/latest/exercises_tools/problem_in_latex.html

.. _Working with Problem Components: http://bharatmooc.readthedocs.org/projects/bharatmooc-partner-course-staff/en/latest/creating_content/create_problem.html

.. _Set the Advertised Start Date: http://bharatmooc.readthedocs.org/projects/bharatmooc-partner-course-staff/en/latest/building_course/setting_up_student_view.html#set-the-advertised-start-date

.. _Full Screen Image: http://bharatmooc.readthedocs.org/projects/bharatmooc-partner-course-staff/en/latest/exercises_tools/full_screen_image.html

.. _Google Instant Hangout Tool: http://bharatmooc.readthedocs.org/projects/bharatmooc-partner-course-staff/en/latest/exercises_tools/google_hangouts.html

.. _Multiple Choice and Numerical Input Problem: http://bharatmooc.readthedocs.org/projects/bharatmooc-partner-course-staff/en/latest/exercises_tools/mult_choice_num_input.html

.. _Components: http://bharatmooc.readthedocs.org/projects/bharatmooc-partner-course-staff/en/latest/creating_content/organizing_course.html#components

.. _Discussions: http://bharatmooc.readthedocs.org/projects/bharatmooc-partner-course-staff/en/latest/running_course/discussions.html  

.. _Guidance for Discussion Moderators: http://bharatmooc.readthedocs.org/projects/bharatmooc-partner-course-staff/en/latest/running_course/discussion_guidance_moderators.html

.. _External Graders: http://bharatmooc.readthedocs.org/projects/bharatmooc-partner-course-staff/en/latest/exercises_tools/external_graders.html

.. _duplicating components: http://bharatmooc.readthedocs.org/projects/bharatmooc-partner-course-staff/en/latest/creating_content/organizing_course.html#duplicate-a-component

.. _Video Advanced Options: http://bharatmooc.readthedocs.org/projects/bharatmooc-partner-course-staff/en/latest/creating_content/create_video.html#advanced-options

.. _Organizaing Your Course Content: http://bharatmooc.readthedocs.org/projects/bharatmooc-partner-course-staff/en/latest/creating_content/organizing_course.html

.. _Custom JavaScript Problem: http://bharatmooc.readthedocs.org/projects/bharatmooc-partner-course-staff/en/latest/exercises_tools/custom_javascript.html

.. _Zooming Image Tool: http://bharatmooc.readthedocs.org/projects/bharatmooc-partner-course-staff/en/latest/exercises_tools/zooming_image.html

.. _IFrame Tool: http://bharatmooc.readthedocs.org/projects/bharatmooc-partner-course-staff/en/latest/exercises_tools/iframe.html

.. _Drag and Drop Problem: http://ca.readthedocs.org/en/latest/exercises_tools/drag_and_drop.html


.. _Assign Discussion Administration Roles: http://bharatmooc.readthedocs.org/projects/bharatmooc-partner-course-staff/en/latest/running_course/discussions.html#assigning-discussion-roles

.. _LTI Component: http://bharatmooc.readthedocs.org/projects/bharatmooc-partner-course-staff/en/latest/exercises_tools/lti_component.html

.. _VitalSource E-Reader Tool: http://bharatmooc.readthedocs.org/projects/bharatmooc-partner-course-staff/en/latest/exercises_tools/vitalsource.html

.. _Problem with Adaptive Hint: http://bharatmooc.readthedocs.org/projects/bharatmooc-partner-course-staff/en/latest/exercises_tools/problem_with_hint.html#problem-with-adaptive-hint


.. _Student Grades and Grading: http://bharatmooc.readthedocs.org/projects/bharatmooc-partner-course-staff/en/latest/running_course/course_grades.html#grades

.. _Answer Data: http://bharatmooc.readthedocs.org/projects/bharatmooc-partner-course-staff/en/latest/running_course/course_answers.html#review-answers

.. _Assign Final Grades and Issue Certificates: http://bharatmooc.readthedocs.org/projects/bharatmooc-partner-course-staff/en/latest/running_course/checking_student_progress.html#checking-student-progress-and-issuing-certificates


.. _Review Grades for Enrolled Students (Small Courses): http://bharatmooc.readthedocs.org/projects/bharatmooc-partner-course-staff/en/latest/running_course/course_grades.html#gradebook

.. DATA DOCUMENTATION

.. _Student Info and Progress Data: http://bharatmooc.readthedocs.org/projects/devdata/en/latest/internal_data_formats/sql_schema.html#student-info

.. _Tracking Logs: http://bharatmooc.readthedocs.org/projects/devdata/en/latest/internal_data_formats/tracking_logs.html#tracking-logs

.. _Discussion Forums Data: http://bharatmooc.readthedocs.org/projects/devdata/en/latest/internal_data_formats/discussion_data.html#discussion-forums-data

.. _Certificate Data: http://bharatmooc.readthedocs.org/projects/devdata/en/latest/internal_data_formats/sql_schema.html#certificates

.. _Wiki Data: http://bharatmooc.readthedocs.org/projects/devdata/en/latest/internal_data_formats/wiki_data.html#wiki-data

.. _Discussion Forum Data: http://bharatmooc.readthedocs.org/projects/devdata/en/latest/internal_data_formats/discussion_data.html#discussion-forums-data

.. _Release Dates: http://bharatmooc.readthedocs.org/projects/bharatmooc-partner-course-staff/en/latest/releasing_course/set_content_releasedates.html#release-dates

.. _Data Czar/Data Team Selection and Responsibilities: http://bharatmooc.readthedocs.org/projects/devdata/en/latest/internal_data_formats/data_czar.html

.. _event list: http://bharatmooc.readthedocs.org/projects/devdata/en/latest/internal_data_formats/event_list.html#event-list

.. Developer Doc

.. _Contributing to Open BharatMOOC: http://bharatmooc.readthedocs.org/projects/userdocs/en/latest/process/index.html

.. _BharatMOOC XBlock Documentation: http://bharatmooc.readthedocs.org/projects/xblock/en/latest/

.. _Analytics: http://bharatmooc.readthedocs.org/projects/userdocs/en/latest/analytics.html

.. _Opaque Keys: https://github.com/bharatmooc/bharatmooc-platform/wiki/Opaque-Keys