.. _nginx: http://nginx.com
.. _gunicorn: http://gunicorn.org
.. _Introduction to the Mac OS X Command Line: http://blog.teamtreehouse.com/introduction-to-the-mac-os-x-command-line
.. _Windows Command Line Reference: http://www.microsoft.com/resources/documentation/windows/xp/all/proddocs/en-us/ntcmds.mspx?mfr=true
.. _Vagrant Getting Started: http://docs.vagrantup.com/v2/getting-started/index.html
.. _VirtualBox: https://www.virtualbox.org/wiki/Downloads
.. _Vagrant: http://www.vagrantup.com/downloads.html
.. _Torrent: https://s3.amazonaws.com/bharatmooc-static/vagrant-images/20140418-injera-devstack.box?torrent
.. _bharatmooc configuration repository: https://github.com/bharatmooc/configuration
.. _bharatmooc configuration repository wiki: https://github.com/bharatmooc/configuration/wiki
.. _Devstack wiki: https://github.com/bharatmooc/configuration/wiki/BharatMOOC-Developer-Stack
.. _Developing on Devstack: https://github.com/bharatmooc/bharatmooc-platform/wiki/Developing-on-the-BharatMOOC-Developer-Stack
.. _RabbitMQ: http://www.rabbitmq.com/
.. _Discern: http://code.bharatmooc.org/discern/
.. _Ease: https://github.com/bharatmooc/ease
.. _VirtualBox Guest Editions: http://www.virtualbox.org/manual/ch04.html
.. _Vagrant documentation: http://docs.vagrantup.com/v2/
.. _Vagrant's documentation on boxes: http://docs.vagrantup.com/v2/boxes.html