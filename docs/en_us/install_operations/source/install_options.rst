.. include:: links.rst

########################################
Open BharatMOOC Platform Installation Options
########################################

You can install the Open BharatMOOC Platform in one of the following three ways.

**********
Devstack
**********

The BharatMOOC Developer Stack, known as **Devstack**, is a Vagrant instance designed
for local development.

Devstack is in the `bharatmooc configuration repository`_ on GitHub.

This guide includes the following sections about Devstack:

* :ref:`Installing the BharatMOOC Developer Stack`

* :ref:`Running the BharatMOOC Developer Stack`
  
Additional sections are planned for future versions of this guide.

See the `bharatmooc configuration repository wiki`_ for information from
BharatMOOC and the Open BharatMOOC community about Devstack and other installation and
configuration options. This wiki contains two pages with more information about Devstack:

* `Devstack wiki`_
* `Developing on Devstack`_


*********************
BharatMOOC Production Stack
*********************

The BharatMOOC Production Stack, known as **Fullstack**, is a Vagrant instance designed
for deploying all BharatMOOC services on a single server.

Fullstack is in the `bharatmooc configuration repository`_ on GitHub.
  
This guide includes :ref:`Installing the BharatMOOC Production Stack`.

See the `bharatmooc configuration repository wiki`_ for information from BharatMOOC and the
Open BharatMOOC community on Fullstack and other installation and configuration
options.

==================
Ubuntu 12.04 64 
==================

You can install the BharatMOOC Production Stack on a single Ubuntu 12.04 64-bit server.

Ubuntu information is planned for future versions of this guide.

You can see the `bharatmooc configuration repository wiki`_ for information from BharatMOOC
and the Open BharatMOOC community about Ubuntu and other installation and configuration
options.