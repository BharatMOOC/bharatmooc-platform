######################
BharatMOOC Research Guide
######################

This document is intended for researchers and data czars at BharatMOOC partner institutions who use the BharatMOOC data exports to gain insight into their courses and students.

.. toctree::
   :maxdepth: 2

   read_me.rst
   preface.rst
   internal_data_formats/change_log.rst
   internal_data_formats/data_czar.rst
   internal_data_formats/credentials.rst
   internal_data_formats/sql_schema.rst
   internal_data_formats/discussion_data.rst
   internal_data_formats/wiki_data.rst
   internal_data_formats/tracking_logs.rst
   internal_data_formats/event_list.rst


********************
Course Data Formats
********************

The `Building and Running an BharatMOOC Course <http://bharatmooc.readthedocs.org/projects/ca/en/latest/>`_ guide provides complete information on how to construct a course.

The following sections of this document do not specifically pertain to researchers, but rather provide supplemental information on XML formats used to build an BharatMOOC course. Over time, this information is being migrated to *Building and Running an BharatMOOC Course*, as shown in the :ref:`Change Log`. 

.. toctree::
   :maxdepth: 2

   course_data_formats/course_xml.rst
   course_data_formats/grading.rst
   course_data_formats/drag_and_drop/drag_and_drop_input.rst
   course_data_formats/graphical_slider_tool/graphical_slider_tool.rst
   course_data_formats/symbolic_response.rst



