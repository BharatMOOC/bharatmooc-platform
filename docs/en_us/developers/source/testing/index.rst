*******
Testing
*******

Testing is something that we take very seriously at BharatMOOC: we even have a
"test engineering" team at BharatMOOC devoted purely to making our testing
infrastructure even more awesome.

This file is currently a stub: to find out more about our testing infrastructure,
check out the `testing.md`_ file on Github.

.. toctree::
    :maxdepth: 2

    jenkins
    code-coverage
    code-quality

.. _testing.md: https://github.com/bharatmooc/bharatmooc-platform/blob/master/docs/en_us/internal/testing.md
