This is the main BharatMOOC platform which consists of LMS and Studio.

See [code.bharatmooc.org](http://code.bharatmooc.org/) for other parts of the BharatMOOC code base.

Installation
------------

Please refer to the following wiki pages in our [configuration repo](https://github.com/bharatmooc/configuration) to install BharatMOOC:

* [BharatMOOC Developer Stack](https://github.com/bharatmooc/configuration/wiki/BharatMOOC-Developer-Stack)
<br/>These instructions are for developers who want to contribute or make changes to the BharatMOOC source code.
* [BharatMOOC Production Stack](https://github.com/bharatmooc/configuration/wiki/BharatMOOC-Production-Stack)
<br/>Using Vagrant/Virtualbox this will setup all BharatMOOC services on a single server in a production like configuration.
* [BharatMOOC Ubuntu 12.04 installation](https://github.com/bharatmooc/configuration/wiki/BharatMOOC-Ubuntu-12.04-Installation)
<br/>This will install BharatMOOC on an existing Ubuntu 12.04 server.


License
-------

The code in this repository is licensed under version 3 of the AGPL unless
otherwise noted. Please see the
[`LICENSE`](https://github.com/bharatmooc/bharatmooc-platform/blob/master/LICENSE) file
for details.

Documentation
------------

Documentation for developers, researchers, and course staff is located in the
`docs` subdirectory. Documentation is built using
[Sphinx](http://sphinx-doc.org/): you can [view the built documentation on
ReadTheDocs](http://docs.bharatmooc.org/).

How to Contribute
-----------------

Contributions are very welcome, but for legal reasons, you must submit a signed
[individual contributor's agreement](http://code.bharatmooc.org/individual-contributor-agreement.pdf)
before we can accept your contribution. See our
[CONTRIBUTING](https://github.com/bharatmooc/bharatmooc-platform/blob/master/CONTRIBUTING.rst)
file for more information -- it also contains guidelines for how to maintain
high code quality, which will make your contribution more likely to be accepted.

Reporting Security Issues
-------------------------

Please do not report security issues in public. Please email security@bharatmooc.org

Mailing List and IRC Channel
----------------------------

You can discuss this code on the [bharatmooc-code Google Group](https://groups.google.com/forum/#!forum/bharatmooc-code) or in the [`bharatmooc-code` IRC channel on Freenode](http://webchat.freenode.net/?channels=bharatmooc-code).
