# We intentionally define lots of variables that aren't used, and
# want to import all variables from base settings files
# pylint: disable=W0401, W0614

# Settings for bharatmooc4bharatmooc production instance
from .aws import *
COURSE_NAME = "bharatmooc4bharatmooc"
COURSE_NUMBER = "BharatMOOC.01"
COURSE_TITLE = "bharatmooc4bharatmooc: BharatMOOC Author Course"
BHARATMOOC4BHARATMOOC_ROOT = ENV_ROOT / "data/bharatmooc4bharatmooc"

### Dark code. Should be enabled in local settings for devel.
ENABLE_MULTICOURSE = True   # set to False to disable multicourse display (see lib.util.views.bharatmoochome)
###
PIPELINE_CSS_COMPRESSOR = None
PIPELINE_JS_COMPRESSOR = None

COURSE_DEFAULT = 'bharatmooc4bharatmooc'
COURSE_SETTINGS =  {'bharatmooc4bharatmooc': {'number' : 'BharatMOOC.01',
                                    'title': 'bharatmooc4bharatmooc: BharatMOOC Author Course',
                                    'xmlpath': '/bharatmooc4bharatmooc/',
                                    'github_url': 'https://github.com/MITx/bharatmooc4bharatmooc',
                                    'active': True,
                                    'default_chapter': 'Introduction',
                                    'default_section': 'bharatmooc4bharatmooc_Course',
                                    },
                    }

STATICFILES_DIRS = [
    PROJECT_ROOT / "static",
    ("bharatmooc4bharatmooc", BHARATMOOC4BHARATMOOC_ROOT / "html"),
    ("circuits", DATA_DIR / "images"),
    ("handouts", DATA_DIR / "handouts"),
    ("subs", DATA_DIR / "subs"),

# This is how you would use the textbook images locally
#    ("book", ENV_ROOT / "book_images"),
]

MAKO_TEMPLATES['course'] = [DATA_DIR, BHARATMOOC4BHARATMOOC_ROOT]
