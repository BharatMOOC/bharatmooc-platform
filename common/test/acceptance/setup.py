#!/usr/bin/env python

"""
Install bok-choy page objects for acceptance and end-to-end tests.
"""

import os
from setuptools import setup

VERSION = '0.0.1'
DESCRIPTION = "Bok-choy page objects for bharatmooc-platform"

# Pip 1.5 will try to install this package from outside
# the directory containing setup.py, so we need to use an absolute path.
PAGES_PACKAGE_DIR = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'pages')

setup(
    name='bharatmoocapp-pages',
    version=VERSION,
    author='BharatMOOC',
    url='http://github.com/bharatmooc/bharatmooc-platform',
    description=DESCRIPTION,
    license='AGPL',
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: GNU Affero General Public License v3',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Topic :: Software Development :: Testing',
        'Topic :: Software Development :: Quality Assurance'
    ],
    package_dir={'bharatmoocapp_pages': PAGES_PACKAGE_DIR},
    packages=['bharatmoocapp_pages', 'bharatmoocapp_pages.lms', 'bharatmoocapp_pages.studio']
)
